#include "NPOS_Sys.h"

//*********************************
//
// This file module contains miscellaneous resources...
// These functions can alter the most eligible task state of
// NPOS, but does *not* invoke NPOS_Schedule() to act on it...
//
//*********************************

//========================

// NPOS_SignalSetInt-
//
// Identical to NPOS_SignalSet() but 
// more suitable to use in an interrupt as it
// does not use/need an exclusion mechanism...
unsigned int NPOS_SignalSetInt(NPOS_Task * TaskToSignal, unsigned int SignalSet)
{
    // Introduce the SignalSet into the specified task's SignalActiveSet...
    TaskToSignal->SignalActiveSet |= SignalSet;
    
    // If the specified task's waitset is satisfied - enable the
    // specified task to run and but do NOT run the scheduler...
    if(TaskToSignal->SignalActiveSet & TaskToSignal->SignalWaitSet)
    {
        TaskControl.TaskActiveSet |= TaskToSignal->TaskMask;
    }
   
    return TaskToSignal->SignalActiveSet;
}

// NPOS_SignalSet-
//
// A watered down version of NPOS_Signal()...
// Only sets signals in the specified task's SignalActiveSet
// and the system active task set-
// Then returns the specified task's SignalActiveSet
// It does not act with the scheduler as does NPOS_Signal()...
// NPOS_NPOS_SignalSet(0) is useful to safely peak at
// a task's active signal set...
unsigned int NPOS_SignalSet(NPOS_Task * TaskToSignal, unsigned int SignalSet)
{
    DISABLE();
    // Introduce the SignalSet into the specified task's SignalActiveSet...
    TaskToSignal->SignalActiveSet |= SignalSet;
    
    // If the specified task's waitset is satisfied - enable the
    // specified task to run and but do NOT run the scheduler...
    if(TaskToSignal->SignalActiveSet & TaskToSignal->SignalWaitSet)
    {
        TaskControl.TaskActiveSet |= TaskToSignal->TaskMask;
    }
    ENABLE();
   
    return TaskToSignal->SignalActiveSet;
}

// NPOS_SignalCheck-
//
// A watered down version of NPOS_Wait()...
// It only checks the calling task's SignalActiveSet against the
// specified WaitSet and returns the signal set that satisfies
// the WaitSet. The scheduler is not used and
// so the task does not block...
unsigned int NPOS_SignalCheck(unsigned int WaitSet)
{
    unsigned int SignalSatisfyMask;
    
    DISABLE();
    // Return the set of signals that satisfy the WaitSet
    // against the SignalActiveSet...
    SignalSatisfyMask = TaskControl.ActiveTask->SignalActiveSet & WaitSet;
    
    // Clear the set of active signals consumed by
    // satisfying our wait check. Return the wait satisfy set...
    TaskControl.ActiveTask->SignalActiveSet &= ~SignalSatisfyMask;
    ENABLE();
    
    return SignalSatisfyMask;
}

// NPOS_Forbid-
//
// The task Forbid field is a nesting counter flag to
// disable task scheduling- This function increments it...
// Tasking is disabled when the counter is not 0...
// Task disabling is task local. A task can break it by
// blocking on a NPOS_NPOS_Wait() and the task disabled
// state restored when the task is running again...
// Returns the current Forbid counter...
unsigned int NPOS_Forbid(void)
{
    TaskControl.ActiveTask->Forbid++;
    
    return TaskControl.ActiveTask->Forbid;
}

// NPOS_Permit-
//
// The task Forbid field is a nesting counter flag to
// disable task scheduling- This function decrements it...
// Returns the current Forbid counter...
unsigned int NPOS_Permit(void)
{
    TaskControl.ActiveTask->Forbid--;
    
    return TaskControl.ActiveTask->Forbid;
}
