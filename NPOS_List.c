#include "NPOS_List.h"

//*********************************
//
// Linked list mangement...
// This provides general purpose linked lists...
// This is a core NPOS resource...
//
//*********************************

//=================================

// Prepare a List_Anchor for use... 
void InitListAnchor(List_Anchor * Anchor)
{
    Anchor->Start.Previous = 0;
    Anchor->Start.Next = &Anchor->End;
    
    Anchor->End.Previous = &Anchor->Start;
    Anchor->End.Next = 0;
}

// Returns 1 if the List_Anchor has no valid List_Nodes...
unsigned int IsListAnchorEmpty(List_Anchor * ValidAnchor)
{
    return ((ValidAnchor->Start.Next == &ValidAnchor->End) ? 1 : 0);
}

// Returns the head valid List_Node...
List_Node * GetListAnchorHead(List_Anchor * ValidAnchor)
{
    return (IsListAnchorEmpty(ValidAnchor) ? 0 : ValidAnchor->Start.Next);
}

// Returns the tail valid List_Node...
List_Node * GetListAnchorTail(List_Anchor * ValidAnchor)
{
    return (IsListAnchorEmpty(ValidAnchor) ? 0 : ValidAnchor->End.Previous);
}

// Place the List_Node as the head valid List_Node...
void AddListAnchorHead(List_Node * Node, List_Anchor * ValidAnchor)
{
    Node->Previous = &ValidAnchor->Start;
    Node->Next = ValidAnchor->Start.Next;
    
    Node->Next->Previous = Node;
    ValidAnchor->Start.Next = Node;
}

// Place the List_Node as the tail valid List_Node...
void AddListAnchorTail(List_Node * Node, List_Anchor * ValidAnchor)
{
    Node->Previous = ValidAnchor->End.Previous;
    Node->Next = &ValidAnchor->End;
    
    Node->Previous->Next = Node;
    ValidAnchor->End.Previous = Node;
}

//=================================

// Returns the previous valid List_Node...
List_Node * GetPreviousListNode(List_Node * ValidNode)
{
    List_Node * Previous = ValidNode->Previous;
    
    return (Previous->Previous ? Previous : 0);
}

// Returns the next valid List_Node...
List_Node * GetNextListNode(List_Node * ValidNode)
{
    List_Node * Next = ValidNode->Next;
    
    return (Next->Next ? Next : 0);
}

// Removes the valid List_Node and glues the list back together...
void RemoveListNode(List_Node * ValidNode)
{
    List_Node * Previous = ValidNode->Previous;
    List_Node * Next = ValidNode->Next;
    
    Previous->Next = Next;
    Next->Previous = Previous;
}

// Inserts the List_Node after the valid List_Node...
void InsertAfterListNode(List_Node * Node, List_Node * ValidNode)
{
    Node->Previous = ValidNode;
    Node->Next = ValidNode->Next;
    
    Node->Next->Previous = Node;
    ValidNode->Next = Node;
}

// Inserts the List_Node before the valid List_Node...
void InsertBeforeListNode(List_Node * Node, List_Node * ValidNode)
{
    Node->Previous = ValidNode->Previous;
    Node->Next = ValidNode;
    
    Node->Previous->Next = Node;
    ValidNode->Previous = Node;
}
