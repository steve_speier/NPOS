#include "Msg.h"

//*********************************
//
// Message and message ports...
// Not a core NPOS resource...
//
//*********************************

//=================================

// A message port must be initialized before any use...
// It is setup to belong to the calling task and will
// be signaled by SignalSet when a message arrives ...
void InitMsgPort(MsgPort * NewMsgPort,
                 unsigned int SignalSet)
{
    // Setup our message list anchor...
    NewMsgPort->List.Start.Previous = 0;
    NewMsgPort->List.Start.Next = &NewMsgPort->List.End;
    
    NewMsgPort->List.End.Previous = &NewMsgPort->List.Start;
    NewMsgPort->List.End.Next = 0;
    
    // Setup this message port with the task signaling mechanism
    // for the active task calling...
    NewMsgPort->TaskToSignal = TaskControl.ActiveTask;
    NewMsgPort->SignalSet = SignalSet;
}

// Gets the next message from the message port if any...
// The message is removed from the message list...
MsgNode * GetMsg(MsgPort * TheMsgPort)
{
    MsgNode * NextMsg = (MsgNode *)GetListAnchorHead((List_Anchor *)TheMsgPort);
    
    if(NextMsg)
    {
        RemoveListNode((List_Node *)NextMsg);
    }
    
    return NextMsg;
}

// Gets the next message from the message port if any...
// The message is *not* removed from the message list...
MsgNode * CheckMsg(MsgPort * TheMsgPort)
{
    return (MsgNode *)GetListAnchorHead((List_Anchor *)TheMsgPort);
}

// Allows for a task to wait until a new message has arrived...
// Returns the signal set that satisfies an NPOS_Wait()...
unsigned int WaitMsg(MsgPort * TheMsgPort)
{
    return NPOS_Wait(TheMsgPort->SignalSet);
}

// A message must be initialized before use...
// The message ports of To and From are used
// to route the message...
// TYpe is user defined and is not used by the services...
// The message structure should be redefined, extended and used
// for user purposes...
void BuildMsg(MsgNode * TheMsg,
              MsgPort * ToMsgPort,
              MsgPort * FromMsgPort,
              unsigned int Type)
{
    TheMsg->To = ToMsgPort;
    TheMsg->From = FromMsgPort;
    TheMsg->Type = Type;
}

// Send the valid message to the specified To message port...
void SendMsg(MsgNode * TheMsg)
{
    AddListAnchorTail((List_Node *)TheMsg, (List_Anchor *)TheMsg->To);
    NPOS_Signal(TheMsg->To->TaskToSignal, TheMsg->To->SignalSet);
}

// Send the valid message to the specified From message port...
void ReplyMsg(MsgNode * TheMsg)
{
    AddListAnchorTail((List_Node *)TheMsg, (List_Anchor *)TheMsg->From);
    NPOS_Signal(TheMsg->From->TaskToSignal, TheMsg->From->SignalSet);
}

