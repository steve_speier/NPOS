#ifndef _NPOS_SYS_H_
#define _NPOS_SYS_H_

//*********************************
//
// NPOS core resources...
//
//*********************************

#include <setjmp.h>

#include "NPOS_cfg.h"

//========================

#define NPOS_VersionMajor   1
#define NPOS_VersionMinor   0

// See NPOS_Signal() and NPOS_Wait() for more details...
// Essentially MORE_COOPERATION insures that NPOS_Schedule()
// is always invoked on NPOS_Signal() and NPOS_Wait() and thus
// helps to insure that any task signals introduced by interrupts
// are acted on a bit more quickly...
#define MORE_COOPERATION    0
#define MORE_PREEMPTIVE     1
#define SCHEDULE_CONTROL    MORE_COOPERATION

//========================

// Debug control- primarily printf()...

#define DEBUG_OFF       0
#define DEBUG_BOOT      (1 << 0)
#define DEBUG_TSKSW     (1 << 1)
#define DEBUG_SCHED     (1 << 2)
#define DEBUG_WAIT      (1 << 3)

//#define DEBUG_MODE (DEBUG_BOOT + DEBUG_TSKSW + DEBUG_SCHED + DEBUG_WAIT)
#define DEBUG_MODE (DEBUG_OFF)

//========================

// Task resources...
typedef struct _NPOS_Task
{
    unsigned int    volatile SignalActiveSet;   // Insure exclusion on use...
    unsigned int    SignalWaitSet;
    unsigned int    SignalAllocSet;
    void            (* TaskFunc)(void);
    unsigned int    Forbid;
    unsigned int    TaskID;
    unsigned int    TaskMask;
    jmp_buf         Startup;
    jmp_buf         Context;
} NPOS_Task;

// System resources...
typedef struct _NPOS_TaskEnv
{
    unsigned int    volatile TaskActiveSet;     // Insure exclusion on use...
    NPOS_Task       * ActiveTask;
    NPOS_Task       * TaskHash[1 << NPOS_TASK_COUNT];
    NPOS_Task       TaskList[NPOS_TASK_COUNT + 1];
    NPOS_Task       BootDummy;
    unsigned int    UINT_BitSize;
} NPOS_TaskEnv;

//========================

// Single instance of NPOS system...
extern NPOS_TaskEnv TaskControl;

//========================

// NPOS bootstrap...
void NPOS_Boot(void);

// Direct task scheduling core support...
// The next 3 functions directly impact task scheduling and
// may cause a task switch within the service...
unsigned int NPOS_Signal(NPOS_Task * TaskToSignal, unsigned int SignalSet);
unsigned int NPOS_Wait(unsigned int WaitSet);
void NPOS_Schedule(void);
// The actual task switch function. Here for completeness,
// normal usage is via NPOS_Schedule(), but needed to implement
// NPOS_Boot() and NPOS_StartTask()...
void NPOS_TaskSwitch(NPOS_Task * TaskToRun);

// Task scheduling modification support, but don't directly
// cause a task switch...
unsigned int NPOS_SignalSet(NPOS_Task * TaskToSignal, unsigned int SignalSet);
unsigned int NPOS_SignalCheck(unsigned int WaitSet);
unsigned int NPOS_Forbid(void);
unsigned int NPOS_Permit(void);

// Task scheduling modification support (interrupts)...
unsigned int NPOS_SignalSetInt(NPOS_Task * TaskToSignal, unsigned int SignalSet);

// Restart/create a NPOS task from another NPOS task...
void NPOS_StartTask(int TaskIndex, void (* TaskFunction)(void));

// Signal allocation support...
unsigned int NPOS_SignalAlloc(void);
unsigned int NPOS_SignalAllocSet(unsigned int SignalMask);
void NPOS_SignalFree(unsigned int SignalMask);

#endif // _NPOS_SYS_H_
