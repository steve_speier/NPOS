#include "NPOS_Sys.h"

//*********************************
//
// This file module contains task signal allocation management...
// Not essential for NPOS - just a useful task signal management utility-
// But is kept a core NPOS resource anyway...
//
//*********************************

//========================

// NPOS_SignalAlloc-
//
// Allocates a single signal and returns its mask- or 0 if no free signal...
unsigned int NPOS_SignalAlloc(void)
{
    unsigned int SignalMask = 0;
    
    for(unsigned int Index = 0; Index < TaskControl.UINT_BitSize; Index++)
    {
        unsigned int NextMask = 1 << Index;
        
        if((NextMask & TaskControl.ActiveTask->SignalAllocSet) == 0)
        {
            SignalMask = NextMask;
            TaskControl.ActiveTask->SignalAllocSet |= SignalMask;
            break;
        }
    }
    
    return SignalMask;
}

// NPOS_SignalAllocSet-
//
// Attempts to allocate the given signal mask. Returns the given signal mask
// if available and has been allocated. Otherwise what was available is
// returned and *no* signals were allocated...
unsigned int NPOS_SignalAllocSet(unsigned int SignalMask)
{
    unsigned int AvailableSet = SignalMask & ~TaskControl.ActiveTask->SignalAllocSet;
    
    if(AvailableSet == SignalMask)
    {
        TaskControl.ActiveTask->SignalAllocSet |= SignalMask;
    }
    
    return AvailableSet;
}

// NPOS_SignalFree-
//
// Return the signal set mask to the free signal pool...
void NPOS_SignalFree(unsigned int SignalMask)
{
    TaskControl.ActiveTask->SignalAllocSet &= ~SignalMask;
}

