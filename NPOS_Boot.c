#include "NPOS_Sys.h"
// Removed to avoid standard library linkage-
// Denote with //$$2021/11/11
//$$2021/11/11#include <stdlib.h>
#include <limits.h>

//*********************************
//
// This file module contains the bootstrap resources of NPOS...
//
//*********************************

#if DEBUG_MODE
#include <stdio.h>
#endif

//========================

// Support functions...
static void StartTasks(void);
static void AllocTasks(unsigned int * HoldInstance);
static void SetupTaskHashTable(void);
static unsigned int GetBitSize(void);
static void NPOS_TaskDefault(void);

//========================

// NPOS_Boot-
//
// Absolute starting point of NPOS-
// Entry is a single thread and that thread
// must have enough stack to accomodate all
// of the NPOS task stacks...
// Each stack should be large enough to allow all
// interrupts and workset operations to be contained
// within its task stack size...
// Total needed stack space is given as:
// ((NPOS_TASK_COUNT + 1) * NPOS_TASK_STACK_INT_SIZE + boot thread stack usage)
// Do not run any NPOS services or allow interrupts that are NPOS compliant
// until after NPOS_Boot()...
// No NPOS task should ever exit their task function.
// The *only* task that will allow a clean return to the
// runtime state past NPOS_Boot() is the background task-
// And this is discouraged...
void NPOS_Boot(void)
{
#if DEBUG_MODE & DEBUG_BOOT
    printf("NPOS_Boot() - Entry...\n");
#endif

    // Get the bit size of an integer...
    TaskControl.UINT_BitSize = GetBitSize();
    
    // Setup most all of the task's fields-
    for(unsigned int Index = 0; Index <= NPOS_TASK_COUNT; Index++)
    {
        NPOS_Task * NextTask = &TaskControl.TaskList[Index];
        
        NextTask->SignalActiveSet = 0;
        NextTask->SignalWaitSet = 0;
        NextTask->SignalAllocSet = 0;
        NextTask->TaskFunc = TaskFuncDefault[Index];
        NextTask->Forbid = 0;
        NextTask->TaskID = Index;
        NextTask->TaskMask = (Index ? (1 << (Index - 1)) : 0);
    }
    
    // Outfit the task lookup hash table...
    SetupTaskHashTable();
    
    // Minimul Boot Dummy task setup...
    TaskControl.BootDummy.TaskID = 99;
    
#if DEBUG_MODE & DEBUG_BOOT
    printf("NPOS_Boot() - BitSize %d, Task hash table, Task fields setup...\n", TaskControl.UINT_BitSize);
#endif
    
    // Continue the boot process by allocation the tasks...
    AllocTasks(0);
    
    // We should never return here!!
    //$$2021/11/11exit(0);
    // Only working possibility is the background task
    // (see NPOS_Boot() docs above)
}

//========================

// Helper function for NPOS_Boot()-
// Invoked by AllocTasks() and finishes up the boot process
// by setting up task scheduling and starting up the tasks...
static void StartTasks(void)
{
    // Denote all tasks in the system are initially eligible to run...
    TaskControl.TaskActiveSet = 0;
    for(unsigned int Index = 1; Index <= NPOS_TASK_COUNT; Index++)
    {
        TaskControl.TaskActiveSet |= TaskControl.TaskList[Index].TaskMask;
    }
    
    // Start up normal running with the highest priority task
    // initially running...
    TaskControl.ActiveTask = &TaskControl.BootDummy;
    NPOS_TaskSwitch(&TaskControl.TaskList[NPOS_TASK_COUNT]);
    
    // Should never get here!
    //$$2021/11/11exit(0);
}

// Helper function for NPOS_Boot()-
// It creates a task's stack, startup context and initial task context.
// It does this through recursive calls that do not return in order to
// hold on to the allocated/created resources...
// This function contains the setjmp() startup context and stack for
// all tasks. The startup context is only a starting context for the
// task's working context. NPOS_TaskSwitch() always holds a task'safely
// setjmp() context when blocked and allows it to resume when it
// is again made active...
static void AllocTasks(unsigned int * HoldInstance)
{
    static unsigned int volatile Index;
    
    // Initial requirement...
    if(HoldInstance == 0)
    {
        Index = 0;
    }
    
    // Part I-
    // Allocate stacks and startup context for all tasks...
    // Done by recursive use of AllocTasks()!!
    if(Index <= NPOS_TASK_COUNT)
    {
        NPOS_Task * volatile NextTask = &TaskControl.TaskList[Index];
        
        // Note the following setjmp is the anchor of a task's
        // stack and startup context...
        if(setjmp(NextTask->Startup) == 0)
        {
            // On the initial startup context-
            // One shot creation and init of stack and startup...
            unsigned int StackAlloc[NPOS_TASK_STACK_INT_SIZE];
            
#if DEBUG_MODE & DEBUG_BOOT
            printf("AllocTasks() - Task %d %X stack alloc %X...\n",
                   Index, (unsigned int)NextTask, (unsigned int)StackAlloc);
#endif

            // Note the recursion that *grows* the stack per task...
            // Also this function never exits so thereby holding
            // all of the allocated task resources in place...
            Index++;
            AllocTasks(StackAlloc);
        }
        else
        {
            // Sunsequent startup context for a task invokes here-
            // First prime the task with a normal task context
            // immediately switch back to the invoking host task...
            NPOS_Task * BackLink = TaskControl.ActiveTask;

            TaskControl.ActiveTask = NextTask;
            NPOS_TaskSwitch(BackLink);
            
#if DEBUG_MODE & DEBUG_BOOT
            printf("AllocTasks() - Task %d %X 1st normal context switch...\n",
                   TaskControl.ActiveTask->TaskID, (unsigned int)TaskControl.ActiveTask);
#endif

            // The first *normal* task context switch continues here-
            // If we have a task funtion of 0 - supply the
            // default task function...
            // Note this will not work for a background task
            // and should not be allowed to happen!
            // Caveat Emptor!
            if(NextTask->TaskFunc == 0)
            {
                NextTask->TaskFunc = NPOS_TaskDefault;
            }
            
            // Continue running at the specified task function...
            NextTask->TaskFunc();
        }
    }
    // Part II-
    // This is only run on the NPOS_TASK_COUNT+1th
    // recursive invoke of this function...
    // Prime the intial task context for all tasks...
    else
    {
        // Use the boot dummy task as the psuedo active task
        // and loop through all of the tasks via a
        // setjump() anchor below and give each task
        // an initial task context...
        TaskControl.ActiveTask = &TaskControl.BootDummy;
        setjmp(TaskControl.BootDummy.Context);
        if(Index--)
        {
#if DEBUG_MODE & DEBUG_BOOT
            printf("AllocTasks() - Task %d %X priming context...\n",
                   Index, (unsigned int)&TaskControl.TaskList[Index]);
#endif

            longjmp((&TaskControl.TaskList[Index])->Startup, 1);
        }
        
        // Start NPOS task system...
        StartTasks();
    }
    
    // Should never get here!
    //$$2021/11/11exit(0);
}

// Helper function for NPOS_Boot() to invoke-
// Sets up a task hash list to select a task to run using the hash key index-
// TaskControl.TaskActiveSet...
static void SetupTaskHashTable(void)
{
    // Task 0 is a given-
    // Always runs when there are no active tasks-
    // (TaskControl.TaskActiveSet == 0)
    TaskControl.TaskHash[0] = &TaskControl.TaskList[0];
    
    // Fill out the task hash list appropriately for the
    // rest of the tasks- Each task requires a power of 2
    // count of task hash entries denoted by its TaskMask value
    // placed starting at the TaskMask value index...
    for(unsigned int Index = 1; Index <= NPOS_TASK_COUNT; Index++)
    {
        NPOS_Task * NextTask = &TaskControl.TaskList[Index];
        unsigned int TaskEntriesCount = NextTask->TaskMask;
        unsigned int HashTableIndex = NextTask->TaskMask;
        
        // Place task address in the span of task hash table entries
        // dictated by its TaskMask...
        while(TaskEntriesCount--)
        {
            TaskControl.TaskHash[HashTableIndex++] = NextTask;
        }
    }
}

// Helper function for NPOS_Boot() to invoke-
// This safely gets the bit size of the largest unsigned integer...
static unsigned int GetBitSize(void)
{
    unsigned int BitCount = 0;
    
    for(unsigned int CheckVal = UINT_MAX; CheckVal != 0; CheckVal >>= 1)
    {
        BitCount++;
    }
    
    return BitCount;
}

// Helper function used by NPOS_Task_CreateStack() only-
// Provides a default task function in case of a 0 specification in
// NPOS_StartTask() or a task entry in the TaskFuncDefault array
// at boot up...
// Note that a 0 default is never allowed for the background task---
static void NPOS_TaskDefault(void)
{
    // We zombie the task...
    NPOS_Wait(0);
}
