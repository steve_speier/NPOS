#include <stdio.h>
#include <stdlib.h>
#include "NPOS_Sys.h"
#include "NPOS_List.h"
#include "Timer.h"
#include "Msg.h"

//*********************************
//
// Just an elementary test.c for NPOS project...
//
//*********************************

// Resources for the task timer system...
List_Anchor WaitList;
WaiterNode Waiter[NPOS_TASK_COUNT + 1];

// Resources for messaging system...
typedef struct NewMsg_
{
    MsgNode StandardMsg;
    clock_t NewTime;
} NewMsg;

MsgPort SimpleMsgPort[NPOS_TASK_COUNT + 1];
NewMsg SimpleMsg[NPOS_TASK_COUNT + 1];

//=================================

// Part I task set...
void GenericTaskI(void)
{
    printf("Hello from task %d\n", TaskControl.ActiveTask->TaskID);
    NPOS_Wait(1);
    
    printf("Task %d continuing...\n", TaskControl.ActiveTask->TaskID);
    
    WaiterNode * TimerWait = &Waiter[TaskControl.ActiveTask->TaskID];
    TimerSetup(2, TaskControl.ActiveTask->TaskID * 1000, TimerWait);
    
    // Simply a timer...
    for(;;)
    {
        TimerStart(TimerWait, &WaitList);
        NPOS_Wait(2);
        
        printf("Task %d awoke from timer...\n", TaskControl.ActiveTask->TaskID);
    }
}

// Part II task set...
void GenericTaskII(void)
{
    printf("Part II hello from task %d\n", TaskControl.ActiveTask->TaskID);
    
    // Setup our message port and our message...
    InitMsgPort(&SimpleMsgPort[TaskControl.ActiveTask->TaskID], 2);
    // The message to BG - from us...
    BuildMsg((MsgNode *)&SimpleMsg[TaskControl.ActiveTask->TaskID],
             &SimpleMsgPort[TaskControl.ActiveTask->TaskID],
             &SimpleMsgPort[0],
             TaskControl.ActiveTask->TaskID);
    
    WaiterNode * TimerWait = &Waiter[TaskControl.ActiveTask->TaskID];
    TimerSetup(4, TaskControl.ActiveTask->TaskID * 500, TimerWait);
    TimerStart(TimerWait, &WaitList);
    
    for(;;)
    {
        // Wait for and/or timer and messages...
        unsigned int SignalSet = NPOS_Wait(6);
        
        // Handle a timer...
        if(SignalSet & 4)
        {
            printf("Task %d awoke from timer...\n", TaskControl.ActiveTask->TaskID);
            TimerStart(TimerWait, &WaitList);
        }
        
        // Handle a message...
        if(SignalSet & 2)
        {
            NewMsg * OurMsg = (NewMsg *)GetMsg(&SimpleMsgPort[TaskControl.ActiveTask->TaskID]);
            
            // Insure the validity of the message...
            if(OurMsg)
            {
                ReplyMsg((MsgNode *)OurMsg);
                printf("Task %d awoke from message - time %u...\n",
                       TaskControl.ActiveTask->TaskID,
                       (unsigned int)OurMsg->NewTime);
            }
            else
            {
                printf("Task %d awoke from *no* message!!...\n", TaskControl.ActiveTask->TaskID);
            }
        }
    }
}

void BackgroundTask(void)
{
    // Greetings...
    printf("Hello from the background task\n");
    
    // Initialize a random number generator...
    srand((unsigned int)time(NULL));
    
    // Setup our message port...
    InitMsgPort(&SimpleMsgPort[0], 0);

    // Setup the wait list system- then let  the tasks continue...
    InitListAnchor(&WaitList);
    for(unsigned int Index = 1; Index <= NPOS_TASK_COUNT; Index++)
    {
        NPOS_Signal(&TaskControl.TaskList[Index], 1);
    }
    
    // Settle into a main loop that is mainly about running the
    // task timer system...
    clock_t TimeStamp = clock();
    unsigned int Count = 0;
    unsigned int LoopCounter = 0;
    while(Count < 99)
    {
        // Run the scheduler at top of loop to enact any changes pending-
        // This after all is mostly a small cooperative tasking system model-
        // Also a good place to put a watchdog...
        LoopCounter++;
        NPOS_Schedule();
        
        // Check the timer list- than enact on all changes...
        NPOS_Forbid();
        CheckTimers(&WaitList);
        NPOS_Permit();
        NPOS_Schedule();
        
        // Here we just have carious to-dos that are kicked off
        // on a second tick interval...
        clock_t NextStamp = clock();
        if(NextStamp - TimeStamp >= CLOCKS_PER_SEC)
        {
            Count++;
            TimeStamp = NextStamp;
            printf("Tick %u...\n", Count);
            
            if(Count % 5 == 0)
            {
                printf("Total loops: %u    Avg per sec: %u...\n", LoopCounter, LoopCounter / Count);
            }
            
            // After a bit- repurpose the tasks to something else...
            if(Count == 18)
            {
                NPOS_Forbid();
                for(unsigned int Index = 1; Index <= NPOS_TASK_COUNT; Index++)
                {
                    NPOS_StartTask(Index, GenericTaskII);
                }
                
                // Cleanup the timer wait list...
                InitListAnchor(&WaitList);
                NPOS_Permit();
                NPOS_Schedule();
            }
            
            // The running tasks should be able to handle messages at this point...
            if(Count > 18)
            {
                unsigned int RandStamp = rand();
                
                // We only allow messages to fire on some precentage of opportunities-
                // Also random on the selection of tasks when a message fire occurs...
                if((RandStamp % 31) < 12)
                {
                    unsigned int Mask = RandStamp % (1 << NPOS_TASK_COUNT);
                    unsigned int BitSelect = 1;
                    unsigned int Index = 1;
                    while(Mask >= BitSelect)
                    {
                        // If a task is selected - gets a message...
                        if(Mask & BitSelect)
                        {
                            NewMsg * OutMsg = &SimpleMsg[Index];
                            OutMsg->NewTime = clock();
                            SendMsg((MsgNode *)OutMsg);
                            
                            NewMsg * BackMsg = (NewMsg *)GetMsg(&SimpleMsgPort[0]);
                            if(BackMsg != OutMsg)
                            {
                                printf("No or invalid response to a message from task %d!!\n", Index);
                            }
                        }
                        
                        // Next task...
                        Index++;
                        BitSelect <<= 1;
                    }
                }
            }
        }
    }
}

//=================================

// The default tasks specified at the time of NPOS booting...
void (* TaskFuncDefault[NPOS_TASK_COUNT + 1])(void) =
{
    BackgroundTask,
    GenericTaskI,
    GenericTaskI,
    GenericTaskI
};

//=================================

// THe main before NPOS...
int main(void)
{
    printf("A NPOS test...\n");
    
    for(unsigned int Count = 1; Count <= 5; Count++)
    {
        //SecWait(1);
        MilliWait(1000);
        printf("%d seconds...\n", Count);
    }
    
    printf("\nBooting NPOS...\n\n");
    NPOS_Boot();
    
    // Normally never get here...
    return 0;
}