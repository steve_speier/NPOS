#ifndef _NPOS_CFG_
#define _NPOS_CFG_

//*********************************
//
// This contains the user defined resources...
//
//*********************************

#define NPOS_TASK_COUNT 3               // Task count + 1 for background task-
#define NPOS_TASK_STACK_INT_SIZE 1024   // Stack size for tasks-

// Index 0 for background task *must* have an entry...
// Index 1..NPOS_TASK_COUNT can be 0 for the default
// system task handler...
extern void (* TaskFuncDefault[NPOS_TASK_COUNT + 1])(void);

// Exclusion macros that need to be defined per platform...
#define DISABLE()
#define ENABLE()

#endif // _NPOS_CFG_
