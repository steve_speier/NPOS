#include "Timer.h"

//*********************************
//
// Contains time wait functions and an elementary millisecond
// task timer...
// Not a core NPOS resource...
//
//*********************************

//=================================

// Setup all of the control fields of a timer.
// The calling active task is assumed to be the task
// to receive a NPOS_SignalSet()...
void TimerSetup(unsigned int SignalSet,
                unsigned int MillisToWait,
                WaiterNode * Waiter)
{
    Waiter->TaskToSignal = TaskControl.ActiveTask;
    Waiter->SignalSet = SignalSet;
    Waiter->MillisToWait = MillisToWait;
}

// The timer is timestamped and placed at the end of the timer WaitList...
void TimerStart(WaiterNode * Waiter, List_Anchor * WaitList)
{
    Waiter->TimeStamp = clock();
    AddListAnchorTail((List_Node *)Waiter, WaitList);
}

// Scans all of the timers on the WaitList. Any timers that have expired
// are removed from the WaitList and the specifed task is signaled using
// NPOS_Signal()...
void CheckTimers(List_Anchor * WaitList)
{
    WaiterNode * NextWaiter = (WaiterNode *)GetListAnchorHead(WaitList);
    
    while(NextWaiter)
    {
        WaiterNode * LookAhead = (WaiterNode *)GetNextListNode((List_Node *)NextWaiter);
        
        if(clock() - NextWaiter->TimeStamp >= NextWaiter->MillisToWait)
        {
            RemoveListNode((List_Node *)NextWaiter);
            NPOS_Signal(NextWaiter->TaskToSignal, NextWaiter->SignalSet);
        }
        
        NextWaiter = LookAhead;
    }
}

//=================================
// Simple busy wait functions...

void SecWait(unsigned int Secs)
{
    clock_t NextTimeStamp = clock();
    for(unsigned int Count = 0; Count < Secs; Count++)
    {
        clock_t TimeStamp = NextTimeStamp;
        while(NextTimeStamp - TimeStamp < CLOCKS_PER_SEC)
        {
            NextTimeStamp = clock();
        }
    }
}

void MilliWait(unsigned int Millis)
{
    clock_t NextTimeStamp = clock();
    for(unsigned int Count = 0; Count < Millis; Count++)
    {
        clock_t TimeStamp = NextTimeStamp;
        while(NextTimeStamp - TimeStamp < CLOCKS_PER_MILLI)
        {
            NextTimeStamp = clock();
        }
    }
}

