#ifndef _MSG_H_
#define _MSG_H_

//*********************************
//
// Set of resource for general purpose messaging...
// Not a core NPOS resource...
//
//*********************************

#include "NPOS_Sys.h"
#include "NPOS_List.h"

//=================================

// The messaging system faciltates the exchange of messages between tasks.
// Message ports belong to tasks that initializes them for use by
// InitMsgPort(). Messaging is designed for having messages sent by SendMsg()
// and then returned by ReplyMsg(), although this is not enforced and arbitrary.
// Messages send or replied are placed on their respective message port lists
// and the owning tasks signaled of a message arrival. A message can only be
// placed on one message port list at a time...

typedef struct _MsgPort
{
    List_Anchor     List;
    NPOS_Task     * TaskToSignal;
    unsigned int    SignalSet;
} MsgPort;

typedef struct _MsgNode
{
    List_Node       Link;
    MsgPort       * To;
    MsgPort       * From;
    unsigned int    Type;
    
// Extend and expand here as needed by application...
// THe above structure must exist first in the new
// structure layout defined...
} MsgNode;

//=================================

// Message port services...
void InitMsgPort(MsgPort * NewMsgPort,
                 unsigned int SignalSet);
MsgNode * GetMsg(MsgPort * TheMsgPort);
MsgNode * CheckMsg(MsgPort * TheMsgPort);
unsigned int WaitMsg(MsgPort * TheMsgPort);

// Message services...
void BuildMsg(MsgNode * TheMsg,
              MsgPort * ToMsgPort,
              MsgPort * FromMsgPort,
              unsigned int Type);
void SendMsg(MsgNode * TheMsg);
void ReplyMsg(MsgNode * TheMsg);

#endif // _MSG_H_
