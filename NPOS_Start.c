#include "NPOS_Sys.h"

//*********************************
//
// This file module contains the task start resources...
//
//*********************************

//========================

// NPOS_StartTask-
//
// The task being started and the task calling this must not be the same!!!
// For a task *other* than the background task (TaskIndex == 0)-
// the TaskFunction specified can be 0 for the default system task function.
// A task function *must* be specified for the background task!!
void NPOS_StartTask(int TaskIndex, void (* TaskFunction)(void))
{
    NPOS_Task * volatile NewTask = &TaskControl.TaskList[TaskIndex];
    
    // Initialize task control signals as well...
    NewTask->SignalWaitSet = 0;
    NewTask->SignalAllocSet = 0;
    NewTask->TaskFunc = TaskFunction;
    NewTask->Forbid = 0;
    
    // Prime the task context...
    // The active task starting up the new task will
    // get back here straight after the new task context prime...
    if(setjmp(TaskControl.ActiveTask->Context) == 0)
    {
        longjmp(NewTask->Startup, 1);
    }
    
    // Run NPOS_Schedule() to reflect
    // the new task environment...
    DISABLE();
    NewTask->SignalActiveSet = 0;
    TaskControl.TaskActiveSet |= NewTask->TaskMask;
    ENABLE();
    NPOS_Schedule();
}
