#include "NPOS_Sys.h"

//*********************************
//
// This file module contains the core task scheduling resources...
//
//*********************************

#if DEBUG_MODE
#include <stdio.h>
#endif

//========================

// NPOS_Signal-
//
// Allows task to signal another task (or the task itself).
// If the signal set satisfies the task's wait set it will then
// be eligible to run and possibly scheduled by this function-
// See NPOS_Wait()
unsigned int NPOS_Signal(NPOS_Task * TaskToSignal, unsigned int SignalSet)
{
    DISABLE();
    // Introduce the SignalSet into the specified task's SignalActiveSet...
    TaskToSignal->SignalActiveSet |= SignalSet;
    
    // If the specified task's waitset is satisfied - enable the
    // specified task to run and run the scheduler...
    if(TaskToSignal->SignalActiveSet & TaskToSignal->SignalWaitSet)
    {
        TaskControl.TaskActiveSet |= TaskToSignal->TaskMask;
        #if SCHEDULE_CONTROL == MORE_PREEMPTIVE
        ENABLE();
        NPOS_Schedule();
        DISABLE();
        #endif
    }
    ENABLE();
    
    #if SCHEDULE_CONTROL == MORE_COOPERATION
    NPOS_Schedule();
    #endif
    return TaskToSignal->SignalActiveSet;
}

// NPOS_Wait-
//
// Allows the current task to continue if the WaitSet ANDed
// with the task's SignalActiveSet is not 0 and this SignalSatisfyMask
// is returned without blocking. Otherwise the task will block until
// an NPOS_Signal() occurs that provides one of more signals that
// will then be the SignalSatisfyMask returned. See NPOS_Signal().
// Note that a NPOS_Wait(0) will block a task forever---
// The background task (task 0) can never wait as it is the floor
// priority task and therefore there is no lower priority task to defer
// execution to- It is the failsafe task to execute if all other tasks
// are waiting...
unsigned int NPOS_Wait(unsigned int WaitSet)
{
    unsigned int SignalSatisfyMask;
    
    DISABLE();
    // Check if we are to block or not...
    TaskControl.ActiveTask->SignalWaitSet = WaitSet;
    SignalSatisfyMask = TaskControl.ActiveTask->SignalActiveSet &
                        TaskControl.ActiveTask->SignalWaitSet;
                        
#if DEBUG_MODE & DEBUG_WAIT
    printf("NPOS_Wait() - Active Task %d %X - Signal Satisfy Set %X...\n",
           TaskControl.ActiveTask->TaskID, (unsigned int)TaskControl.ActiveTask,
           SignalSatisfyMask);
#endif

    if(SignalSatisfyMask == 0)
    {
        // We are to block - run some other task until we are
        // signaled with signals that satisfy our wait...
        TaskControl.TaskActiveSet &= ~TaskControl.ActiveTask->TaskMask;
        ENABLE();
        NPOS_Schedule();
        DISABLE();
        
        // We have been awoken! Get the signals satisfying our wait...
        SignalSatisfyMask = TaskControl.ActiveTask->SignalActiveSet &
                            TaskControl.ActiveTask->SignalWaitSet;
    }
    
    // Clear our waitset and the set of active signals consumed by
    // satisfying our wait. Return the wait satisfy set...
    TaskControl.ActiveTask->SignalWaitSet = 0;
    TaskControl.ActiveTask->SignalActiveSet &= ~SignalSatisfyMask;
    ENABLE();
    
    #if SCHEDULE_CONTROL == MORE_COOPERATION
    NPOS_Schedule();
    #endif
    return SignalSatisfyMask;
}

// NPOS_Schedule-
//
// Task scheduling performed so that the highest eligible to run task
// is always running. Uses NPOS_TaskSwitch() to swap about task execution
// and is the only place NPOS_TaskSwitch() should be used-
void NPOS_Schedule(void)
{
    DISABLE();
    NPOS_Task * MostEligibleTask = TaskControl.TaskHash[TaskControl.TaskActiveSet];
    ENABLE();
    
#if DEBUG_MODE & DEBUG_SCHED
    printf("NPOS_Schedule() - Active Task %d %X - ",
           TaskControl.ActiveTask->TaskID, (unsigned int)TaskControl.ActiveTask);
    printf("Most Elgible Task %d %X...\n",
           MostEligibleTask->TaskID, (unsigned int)MostEligibleTask);
#endif

    // Only need to do anything if the mast eligible task is now
    // different from the active task...
    if(TaskControl.ActiveTask != MostEligibleTask)
    {
        // Check if the active task has forbidden task switching...
        if(TaskControl.ActiveTask->Forbid)
        {
            // Tasking is forbidden- we will hold off eligible
            // higher priority tasks...
            if(MostEligibleTask->TaskMask < TaskControl.ActiveTask->TaskMask)
            {
                // BUT --- if the most eligible task is now a *lower* priority-
                // we allow the task switch as the active task *must* be
                // at a NPOS_Wait() and the lower priority most eligible
                // is allowed to become active...
                NPOS_TaskSwitch(MostEligibleTask);
            }
        }
        else
        {
            // Not forbidding tasking- the most eligible task always
            // becomes the new active task...
            NPOS_TaskSwitch(MostEligibleTask);
        }
    }
}

// NPOS_TaskSwitch-
//
// Normal NPOS operation task switching portal that all tasks
// context switch through- The ActiveTask allows the
// specified TaskToRun to become the current ActiveTask...
// Insure ActiveTask != TaskToRun!
// This function is exported for completeness and for rare occasions
// outside of its use in NPOS_Schedule() that it can be useful-
// (such as NPOS booting)...
void NPOS_TaskSwitch(NPOS_Task * TaskToRun)
{
#if DEBUG_MODE & DEBUG_TSKSW
    printf("NPOS_TaskSwitch() - From Task %d %X to Task %d %X...\n",
           TaskControl.ActiveTask->TaskID, (unsigned int)TaskControl.ActiveTask,
           TaskToRun->TaskID, (unsigned int)TaskToRun);
#endif

    if(setjmp(TaskControl.ActiveTask->Context) == 0)
    {
        // The ActiveTask has saved its context
        // and now invokes the TaskToRun...
        // Denote TaskToRun as the new ActiveTask...
        TaskControl.ActiveTask = TaskToRun;
        longjmp(TaskToRun->Context, 1);
        
        // Execution should never get here!!!
    }
    
    // The TaskToRun is restarted from its previous
    // suspension at this point in this function and
    // continues on after this function's return...
}
