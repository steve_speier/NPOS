#ifndef _NPOS_LIST_H_
#define _NPOS_LIST_H_

//*********************************
//
// Linked list mangement...
//
// This provides general purpose linked lists...
// This is a core NPOS resource...
//
//*********************************

//=================================

typedef struct _List_Node
{
    struct _List_Node * Previous;
    struct _List_Node * Next;
} List_Node;

typedef struct _List_Anchor
{
    List_Node Start;
    List_Node End;
} List_Anchor;

//=================================

// Services involving a list anchor...
void InitListAnchor(List_Anchor * Anchor);
unsigned int IsListAnchorEmpty(List_Anchor * ValidAnchor);

// Services involving a list anchor and a list node...
List_Node * GetListAnchorHead(List_Anchor * ValidAnchor);
List_Node * GetListAnchorTail(List_Anchor * ValidAnchor);
void AddListAnchorHead(List_Node * Node, List_Anchor * ValidAnchor);
void AddListAnchorTail(List_Node * Node, List_Anchor * ValidAnchor);

// Services involving list nodes only...
List_Node * GetPreviousListNode(List_Node * ValidNode);
List_Node * GetNextListNode(List_Node * ValidNode);
void RemoveListNode(List_Node * ValidNode);
void InsertAfterListNode(List_Node * Node, List_Node * ValidNode);
void InsertBeforeListNode(List_Node * Node, List_Node * ValidNode);

#endif // _NPOS_LIST_H_
