#ifndef _TIMER_H_
#define _TIMER_H_

//*********************************
//
// Contains time wait functions and an elementary millisecond
// task timer...
// Not a core NPOS resource...
//
//*********************************

#include <time.h>
#include "NPOS_Sys.h"
#include "NPOS_List.h"

#define CLOCKS_PER_MILLI (CLOCKS_PER_SEC / 1000)

//=================================

// Task timers are maintained on a wait list. A timer can be setup
// by the active task with TimerSetup(). It can then be made
// active and placed on a wait list by the same active task
// with TimerStart(). The wait list is monitored by some running entity
// with CheckTimers() for any time expirations. When a timer expiration
// occurs, the timer is unlinked from the wait list and the specified
// task is signaled with NPOS_Signal()...

typedef struct _WaiterNode
{
    List_Node       Link;
    NPOS_Task     * TaskToSignal;
    unsigned int    SignalSet;
    clock_t         MillisToWait;
    clock_t         TimeStamp;
} WaiterNode;

void TimerSetup(unsigned int SignalSet,
                unsigned int MillisToWait,
                WaiterNode * Waiter);
void TimerStart(WaiterNode * Waiter, List_Anchor * WaitList);
void CheckTimers(List_Anchor * WaitList);

//=================================
// Simple busy wait functions...
void SecWait(unsigned int Secs);
void MilliWait(unsigned int Millis);

#endif // _TIMER_H_
