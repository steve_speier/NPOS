# NPOS

This is a simple dynamic state machine implemented with a setjmp()/longjmp() task model. Very basic, small, portable, and easy to understand and use. No provisions for interrupt state input- left to the system implementer.